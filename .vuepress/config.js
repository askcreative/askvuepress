module.exports = {
  serviceWorker: 'true',
  title: "Amy Storm Kosman",
  description: "creative",
  head: [
    'link',
    {
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }
  ],
  themeConfig: {
    logo: '/logo.png',
    nav: [{
        text: 'Home',
        link: '/'
      },
      {
        text: 'About',
        link: '/about/'
      },
      {
        text: 'Blog',
        link: '/blog/'
      },
      {
        text: 'Contact',
        link: '/contact/'
      },
      {
        text: 'Gallery',
        link: '/gallery/'
      },
      {
        text: 'Info',
        link: '/info/'
      },
      {
        text: 'Shop',
        link: '/shop/'
      }
    ],
    sidebar: {
      '/info/': [
        '',
        'cookies',
        'payments',
        'privacy',
        'returns',
        'shipping',
        'terms'
      ]
    },
    lastUpdated: 'Last Updated', // string | boolean
  }
}