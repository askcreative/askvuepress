---
sidebar: auto
---
<section class="hero">
  <h1 class="fancyh1 title is-size-1">
    About
  </h1>
  <h2 class="subtitle is-size-3">
  All about Amy and the business.
  </h2>
</section>

## The Business
#### Amy Storm Kosman creative

In 2016 I began to think I was ready to start a small business again. I thought of my passions and began to explore fashion accessories. I experimented with many DIY tutorials and books. I also read up on the environmental impact of the fashion industry. Environmental issues have always been an interest of mine. It seems to me that the huge mess we make on this Earth will have to be cleaned up eventually by someone. I try not to add to that eventual burden. I also think there are many gorgeous vintage fabrics out there.  

I decided my business would strive to diminish the amount of wasted fabric by using the environmental 3 R's: reduce, reuse, and renew as well as embracing the Japanese culture of mottainai. By artfully restoring the beauty of vintage fabrics, and discarded textiles into fashionable accessories I can inspire a wardrobe with creativity and personality. Creating handmade accessories is a practical application of the sustainable fashion movement. It's also fun to make (and shop for).  

## The Person
#### Amy Lynn Storm Kosman ...

is from California and currently living in the Netherlands. I love my family, my parents, my older sister and younger brother, sometimes my husband and our two cats. 

##### Never bored ...

I am always puzzling on the hows in making things, and often resulting in breaking things (I've an old laptop and phone just for OS experiments). I can get overly obsessed puzzling. If I say I'm bored then I'm in need of food or a nap. I'm a long-time napper. 1530 or 1600 is sleepy time.

##### Never boring ...

Well almost never. Actually, probably usually boring. I am the worst at storytelling and communication. This is my lifelong struggle :smile: and everyone else's lifelong bane, to listen to and suffer my stories.

## Timeline

<AboutTimeLine />
